'''
@author: ahmed allam <ahmed.allam@yale.edu>
'''
from pyseqlab.attributes_extraction import GenericAttributeExtractor

class CONLL00_SeqAttributeExtractor(GenericAttributeExtractor):
    """class implementing observation functions that generates attributes from tokens/observations"""

    def __init__(self):
        attr_desc = self.generate_attributes_desc()
        super().__init__(attr_desc)
        
    def generate_attributes_desc(self):
        attr_desc = {}
        attr_desc['w'] = {'description':'the word/token',
                          'encoding':'categorical'
                         }

        attr_desc['pos'] = {'description':'the part-of-speech of the word',
                            'encoding':'categorical'
                           }  
        return(attr_desc)

def example():
    from pyseqlab.utilities import DataFileParser
    import os
    current_dir = os.path.dirname(os.path.realpath(__file__))
    root_dir = os.path.abspath(os.path.join(current_dir, os.pardir))
    dataset_dir = os.path.join(root_dir, "dataset")
    # initialize a data file parser
    dparser = DataFileParser()
    # provide the options to parser such as the header info, the separator between words and if the y label is already existing
    # main means the header is found in the first line of the file
    header = "main"
    # y_ref is a boolean indicating if the label to predict is already found in the file
    y_ref = True
    # spearator between the words/observations
    column_sep = " "
    seqs = []
    # read only one sequence
    for seq in dparser.read_file(os.path.join(dataset_dir, 'train.txt'), header, y_ref=y_ref, column_sep = column_sep):
        seqs.append(seq)
        break
    attr_extractor = CONLL00_SeqAttributeExtractor()
    print("attr_desc {}".format(attr_extractor.attr_desc))
    attr_extractor.generate_attributes(seq, seq.get_y_boundaries())
    for boundary, seg_attr in seq.seg_attr.items():
        print("boundary {}".format(boundary))
        print("attributes {}".format(seg_attr))
    print("seg_attr {}".format(seq.seg_attr))
    return(seq)
    
if __name__ == "__main__":
    example()

