Chunker
=======
To illustrate the use of `PySeqLab package <https://bitbucket.org/A_2/pyseqlab>`__ in training a ``chunker`` using `CoNLL 2000 dataset <http://www.cnts.ua.ac.be/conll2000/chunking/>`__.
Navigate to the ``tutorials`` folder to explore the notebooks for building, training and using ``chunker``. A rendered html version of the tutorial is available on `PySeqLab applications page <http://pyseqlab.readthedocs.io/en/latest/applications.html>`__.