---Model training-- starting time 2016-12-21 16:37:06.148873 
model name: 2016_12_21-16_37_6_147704.model 
model type: <class 'pyseqlab.fo_crf_model.FirstOrderCRF'> 
training method: SGA 
type of regularization: l2 
value of regularization: 1 
learning rate schedule: t_inverse 
eta0: 0.1 
a: 0.9 
number of epochs: 10