Evaluation
==========

To evaluate the learned model on the testing data, navigate to ``eval_model`` folder
and use the ``conlleval.pl`` script on ``dec_testseqs.txt`` in a terminal(shell) ::

	> ./conlleval.pl < dec_testseqs.txt
	
Moreover, the file ``decoded_test_beamsize_5.txt`` represents the decoded test sequences when using the learned model
with ``beam size = 5``.

Same is applied for the decoded training sequences that are found in ``dec_trainseqs_fold_0.txt`` file.

Current results (test set decoded with full beam)
--------------------------------------------------

::

	processed 47377 tokens with 23852 phrases; found: 23791 phrases; correct: 22312.
	accuracy:  95.98%; precision:  93.78%; recall:  93.54%; FB1:  93.66
	             ADJP: precision:  79.10%; recall:  72.60%; FB1:  75.71  402
	             ADVP: precision:  82.19%; recall:  80.48%; FB1:  81.33  848
	            CONJP: precision:  71.43%; recall:  55.56%; FB1:  62.50  7
	             INTJ: precision: 100.00%; recall:  50.00%; FB1:  66.67  1
	              LST: precision:   0.00%; recall:   0.00%; FB1:   0.00  0
	               NP: precision:  94.26%; recall:  93.95%; FB1:  94.11  12380
	               PP: precision:  96.91%; recall:  97.80%; FB1:  97.35  4855
	              PRT: precision:  80.81%; recall:  75.47%; FB1:  78.05  99
	             SBAR: precision:  88.87%; recall:  85.05%; FB1:  86.91  512
	               VP: precision:  93.47%; recall:  94.05%; FB1:  93.76  4687
	               