'''
@author: ahmed allam <ahmed.allam@yale.edu>
'''

import os
from pyseqlab.features_extraction import FOFeatureExtractor
from pyseqlab.fo_crf import FirstOrderCRF, FirstOrderCRFModelRepresentation
from pyseqlab.workflow import  GenericTrainingWorkflow
from pyseqlab.utilities import TemplateGenerator, ReaderWriter, generate_datetime_str, \
                               generate_trained_model, create_directory
from attr_extractor import CONLL00_SeqAttributeExtractor
import numpy
current_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.abspath(os.path.join(current_dir, os.pardir))

        
def load_DataFileParser_options():
    data_parser_options = {'header':'main',
                           'y_ref':True,
                           'column_sep':" ",
                           'seg_other_symbol':None
                           }
    return(data_parser_options)

def template_config():
    template_generator = TemplateGenerator()
    templateXY = {}
    # generating template for w (i.e. word track)
    template_generator.generate_template_XY('w', ('1-gram', range(-2,3)), '1-state:2-states', templateXY)
    template_generator.generate_template_XY('w', ('2-gram', range(-1,2)), '1-state:2-states', templateXY)

    # generating template for pos (i.e. part-of-speech track)
    template_generator.generate_template_XY('pos', ('1-gram:2-gram:3-gram', range(-2,3)), '1-state:2-states', templateXY)

    templateY = {'Y':()}
    return(templateXY, templateY)

def run_training(optimization_options, template_config, train_fname, test_fname, num_seqs=numpy.inf, profile=False):
    import cProfile
    if(profile):
        local_def = {'optimization_options':optimization_options,
                     'template_config':template_config,
                     'train_fname':train_fname,
                     'test_fname':test_fname,
                     'num_seqs':num_seqs
                    }
        global_def = {'train_crfs':train_crfs}
        uid = generate_datetime_str()
        profiling_dir = create_directory('profiling', root_dir)
        cProfile.runctx('train_crfs(optimization_options, template_config, train_fname, test_fname, num_seqs=num_seqs)',
                        global_def, local_def, filename = os.path.join(profiling_dir, "timeprofile_{}".format(uid)))
    else:
        return(train_crfs(optimization_options, template_config, train_fname, test_fname, num_seqs=num_seqs))

def revive_learnedmodel(model_dir):
    modelparts_dir = os.path.join(model_dir, "model_parts")
    lmodel = generate_trained_model(modelparts_dir, CONLL00_SeqAttributeExtractor)
    return(lmodel)
    

def train_crfs(optimization_options, template_config, train_fname, test_fname, num_seqs=numpy.inf):
    crf_model = FirstOrderCRF 
    model_repr = FirstOrderCRFModelRepresentation
    # init attribute extractor
    conll_attrextractor = CONLL00_SeqAttributeExtractor()
    # get the attribute description 
    attr_desc = conll_attrextractor.attr_desc
    
    # load templates
    template_XY, template_Y = template_config()
    # init feature extractor
    fextractor = FOFeatureExtractor(template_XY, template_Y, attr_desc)
    # no need for feature filter
    fe_filter = None

    # create a root directory for processing the data
    wd = create_directory('wd', root_dir)
    workflow_trainer = GenericTrainingWorkflow(conll_attrextractor, fextractor, fe_filter, 
                                               model_repr, crf_model,
                                               wd)
    

    # define the data split strategy
    # since data is already organized in separate files (train and test)
    # we use all data in training file 
    dsplit_options = {'method':"none"}
    # since we are not going to train using perceptron, we can safely setup full_parsing=False
    full_parsing = False

    # load data_parser_options
    data_parser_options = load_DataFileParser_options()
    # load train file
    train_file = os.path.join(root_dir, "dataset", train_fname)
    data_split = workflow_trainer.seq_parsing_workflow(dsplit_options,
                                                       seq_file=train_file,
                                                       data_parser_options=data_parser_options,
                                                       num_seqs=num_seqs,
                                                       full_parsing = full_parsing)
    trainseqs_id = data_split[0]['train']
    crf_m = workflow_trainer.build_crf_model(trainseqs_id,
                                             "f_0", 
                                             full_parsing=full_parsing)
    model_dir = workflow_trainer.train_model(trainseqs_id, 
                                             crf_m, 
                                             optimization_options)
    use_options_seqsinfo = {'seqs_info':workflow_trainer.seqs_info,
                            'model_eval':False,
                            'file_name':'train_fold_0.txt',
                            'sep':' '
                           }
    model_train_perf = workflow_trainer.use_model(model_dir, use_options_seqsinfo)  
    print(model_train_perf)
        
    # get test file
    test_file = os.path.join(root_dir, "dataset", test_fname)
    # define the options to use the model while providing a sequence file
    use_options_seqfile = {'seq_file':test_file,
                           'data_parser_options':data_parser_options,
                           'num_seqs':num_seqs,
                           'model_eval':False,
                           'file_name':'test_fold_0.txt',
                           'sep':' '
                          }

    model_test_perf = workflow_trainer.use_model(model_dir, use_options_seqfile)
    print(model_test_perf)
    
    return((model_dir, model_train_perf, model_test_perf))

if __name__ == "__main__":
    pass
    
    